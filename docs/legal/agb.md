# AGB


## 1 Geltungsbereich

Diese allgemeinen Geschäftsbedingungen gelten für die von DasDomainDepot.de GmbH auf seinem Internet-Portal (www.gigrawars.de), seinen Internetseiten und in App-Stores („Internetseiten”) angebotenen Spiele und sonstige Dienstleistungen („Services”).

Nachfolgend wird die DasDomainDepot.de GmbH meist mit dem Namen GigraWars beschrieben, um den Geltungsbereich klar zu formulieren.

Zu den Services gehören z.B. das Herunterladen, das Installieren und die Teilnahme an den Spielen, das Erstellen eines Kundenkontos, das Erstellen einer Profilseite, die Teilnahme an Blogs und Foren, das Einbringen von Medieninhalten wie Fotos, Texten und Spielen, der Erwerb von virtuellen Gegenständen und sonstige Dienstleistungen. Die Teilnehmer der Spiele von GigraWars und die Nutzer der Internetseiten werden im Folgenden als „Nutzer” bezeichnet. Sofern in diesen Allgemeinen Geschäftsbedingungen („AGB”) von „Spiele(n)” oder „Services” die Rede ist, werden damit die von GigraWars angebotenen Spiele und Services bezeichnet. Für die Geschäftsbeziehungen zwischen GigraWars und den Nutzern gelten ausschließlich diese AGB, sowie auch die Datenschutzerklärung und das Impressum, welche einen integralen Bestandteil der AGB darstellen.

Die Vertragspartner sind die DasDomainDepot.de GmbH und der Nutzer. Die Allgemeinen Geschäftsbedingungen (AGB) des Nutzers sind ausdrücklich nicht Bestandteil des Vertrages, es sei denn, dass GigraWars diesen schriftlich und ausdrücklich zustimmt.

Gegenstand dieser AGB sind nicht etwaige Fragen, die sich aus der Nutzung der Internetseiten und der Spiele mit der Hardware (z.B. PC, Apple, Mobiltelefone, Tablets) oder Software Dritter (z.B. Betriebssystem, Webbrowser, Zugangssoftware), der Nutzung von App-Stores Dritter (z.B. Google Play, iTunes, Windows Store) oder aus der Internet-Verbindung des Nutzers zu den Services stellen können, da diese Leistungen nicht von GigraWars erbracht werden.


### 1.1 Nutzer

**1.1.1** GigraWars richtet seine Spiele und Services ausschließlich an Verbraucher im Sinne des § 13 des Bürgerlichen Gesetzbuchs (BGB). Die Nutzung der Spiele und Services zu anderen Zwecken als privaten Unterhaltungszwecken, insbesondere zu Erwerbs- oder anderen gewerblichen Zwecken, ist ausgeschlossen.

**1.1.2** Berechtigt zur Nutzung sind Volljährige gemäß § 2 des Bürgerlichen Gesetzbuchs (BGB)  oder minderjährige Personen, deren gesetzliche Vertreter der Nutzung im Sinne des § 107 BGB eingewilligt haben.

**1.1.3** Durch die Anmeldung zur Nutzung und/oder die Installation der Spiele und/oder Services versichert der Nutzer ausdrücklich (i) seine Volljährigkeit und Geschäftsfähigkeit oder (ii) im Falle von Minderjährigen, dass die Zustimmung des gesetzlichen Vertreters vorliegt.

Ist die minderjährige Person durch Vollendung des 18. Lebensjahr unbeschränkt geschäftsfähig geworden, so wird die Fortsetzung der Nutzung als gleichzeitige Genehmigung im Sinne des § 108 Abs. 3 BGB aller zuvor im Zusammenhang mit dem Nutzungsvertrag abgegebenen Willenserklärungen des Nutzers betrachtet, sofern die Genehmigung nicht innerhalb von zwei (2) Wochen nach diesem Datum ausdrücklich in Textform gegenüber GigraWars verweigert wird.

### 1.2 Vertragsgegenstand, Nutzung und Änderung der Spiele und Services

**1.2.1** GigraWars ermöglicht den Nutzern innerhalb der verfügbaren technischen und betrieblichen Möglichkeiten die Teilnahme an den Spielen und Services.

**1.2.2** Die Teilnahme an den Spielen dient ausschließlich der Unterhaltung.

**1.2.3** GigraWars stellt auf den Internetseiten bestimmte Informationen zur Verfügung und ermöglicht den Nutzern unter anderem, individuelle persönliche Profile zu erstellen. Diese Profile können von Spielnutzern, Servicenutzern und Dritten eingesehen werden. Des Weiteren ermöglicht GigraWars den Nutzern über die Internetseiten mit Dritten zu kommunizieren, Fotos, Videos, Spiele und, soweit möglich, Musik sowie andere Medieninhalte auszutauschen. Zusätzlich können Nutzer Blogs und Kommentare veröffentlichen sowie Bewertungen über Spiele, andere Nutzer, Dritte oder Transaktionen abgeben.

**1.2.4** Die Spiele von GigraWars stellen eine Dienstleistung dar, die darin besteht, die Spielesoftware in ihrer jeweiligen Form auf den Plattformen von GigraWars oder Dritten während der Vertragslaufzeit bereitzustellen. Die Nutzung wird ermöglicht, indem GigraWars selbst oder durch autorisierte Dritte die erforderliche Anwendung für die Teilnahme auf den entsprechenden URLs der Spiele bzw. Services online zur Verfügung stellt. Ein (Lizenz-)Erwerb von Spielesoftware und/oder einzelner Elemente ist, sofern nicht ausdrücklich anders geregelt, ausgeschlossen. GigraWars behält sich vor, die Spiele auch auf Datenträgern anzubieten, die käuflich erworben werden können, möglicherweise mit einem anderen Funktionsumfang. Der Verkauf von Spielen auf diese Weise unterliegt nicht diesen Geschäftsbedingungen.

**1.2.5** Die Teilnahme an vielen Spielen und die Nutzung bestimmter Services stehen ausschließlich Personen offen, die zuvor durch eine Registrierung ein Kundenkonto (im Folgenden "Account") erstellt haben. Ein bei GigraWars im Rahmen der Services erstellter Account ermöglicht dem Nutzer unter den Bedingungen von Abschnitt 1.1 die Nutzung jedes Spiels. Nach erfolgreicher Account-Erstellung kann der Nutzer am jeweiligen Spiel oder Service teilnehmen, indem er die entsprechende Internetseite aufruft und sich dann einloggt.

**1.2.6** Ein Nutzer hat das Recht, mehrere Accounts anzulegen. Es ist jedoch zu beachten, dass an einigen der von GigraWars betriebenen Spielen die Teilnahme des Nutzers nur mit einem einzigen Account gestattet ist („Verbot von Multiaccounts“). Genauere Informationen dazu sind den jeweiligen Spielregeln zu entnehmen. GigraWars legt Nutzern, die über mehrere Accounts verfügen, nahe, die Spielregeln sorgfältig zu studieren, da ein Verstoß gegen das Verbot von Multiaccounts zu einer sofortigen Sperrung des Nutzers führen kann.

Selbst in Spielen, bei denen einem Nutzer gestattet ist, mehrere Accounts zu besitzen, ist es strengstens untersagt, dass mehrere Accounts desselben Nutzers miteinander kommunizieren oder auf andere Weise interagieren („Verbot von Pushing“). Insbesondere ist es nicht erlaubt, einen Account zu nutzen, um einem anderen Account desselben Nutzers Vorteile zu verschaffen. Dies könnte beispielsweise durch die Übertragung von Gegenständen oder Spielewährung innerhalb des Spiels von einem Account auf einen anderen Account desselben Nutzers erfolgen oder durch die Teilnahme eines Accounts mit oder gegen einen anderen Account desselben Nutzers in einem Wettkampf.

**1.2.7** Das Verbot von Pushing erstreckt sich auch auf Accounts verschiedener Nutzer. Es ist untersagt, dass ein Account dazu genutzt wird, einem Account eines anderen Nutzers durch beispielsweise die Übertragung von Gegenständen oder Spielwährung oder durch absichtliches Verlieren eines Kampfes unredlich Vorteile zu verschaffen (sog. „Boosting“).

**1.2.8** Die Nutzung von Programmen, die eine übermäßige Belastung der Services verursachen, ist untersagt. Ebenfalls unzulässig ist der Einsatz jeglicher Software, die den regulären Spielablauf beeinflusst, insbesondere Programme zur systematischen oder automatischen Steuerung der Spiele oder einzelner Spielfunktionen (wie Bots oder Makros). Jegliche Reproduktion, Auswertung oder Veränderung der Spiele, Spielelemente oder der im Rahmen der Services bereitgestellten Inhalte ist ebenfalls untersagt.

**1.2.9** Es ist untersagt, Bugs oder Fehler in der Programmierung für eigene Zwecke auszunutzen. Im Falle der Entdeckung von Bugs oder Fehlern wird empfohlen, diese umgehend über den Support zu melden.

**1.2.10** Es ist untersagt, die Spiele und Services unter Verwendung von Anonymisierungsdiensten (wie beispielsweise Proxys) oder anderen Methoden zu nutzen, die den Wohnort oder den gewöhnlichen Aufenthaltsort des Nutzers verschleiern.

**1.2.11** Der Nutzer hat keinen Anspruch auf die Eröffnung eines Accounts oder die Veröffentlichung etwaiger Inhalte im Rahmen der Services.

**1.2.12** Für das Spielen und die Teilnahme an den Services wird die Nutzung von aktuellen Browsern und Betriebssystemen vorausgesetzt.

**1.2.13** Die Spiele und Services werden fortlaufend aktualisiert, angepasst, erweitert und verändert. Daher beschränkt sich die Spielberechtigung des Nutzers auf das jeweilige Spiel und die Services in ihrer aktuellen Ausführung.

**1.2.14** Die Grundversion der Spiele kann kostenlos genutzt werden. Allerdings stehen bestimmte Funktionen nur zahlenden Nutzern zur Verfügung (siehe hierzu Punkt 7). Sofern nicht in den Beschreibungen der jeweiligen Services anders vereinbart, ist die Nutzung der Services kostenfrei.

**1.2.15** Der Nutzer hat keinen Anspruch darauf, dass die Spiele oder Services in der bei Vertragsschluss bestehenden Version aufrechterhalten werden. GigraWars behält sich das Recht vor, den Betrieb eines Spiels oder Services jederzeit ohne Angabe von Gründen einzustellen. In einem solchen Fall kann der Nutzer nach Wahl von GigraWars verlangen, dass bereits im Voraus für zeitlich befristete Dauerschuldverhältnisse geleistete Entgelte (z.B. Entgelte für Premium-Mitgliedschaften, Abonnements) für andere Spiele oder Services seiner Wahl gutgeschrieben werden oder dass im Voraus bezahlte Entgelte zurückerstattet werden. Dieses Recht gilt nicht für bereits vollständig geleistete Entgelte im Rahmen von einzelnen Schuldverhältnissen (z.B. Einzelbestellungen). Das Recht des Nutzers, den Vertrag im Falle nicht nutzbarer Spiele oder Services mit sofortiger Wirkung zu kündigen, bleibt davon unberührt. Weitere Ansprüche des Nutzers sind ausgeschlossen, sofern nicht ausdrücklich anders in diesen AGB festgelegt.

## 2 Vertragsangebot und Vertragsschluss
**2.1** Indem der Nutzer das Registrierungsformular ausfüllt oder ein Spiel herunterlädt und installiert, gibt er ein verbindliches Angebot zum Abschluss eines Spiele- und Servicevertrages ab, auch als "Nutzerantrag" bezeichnet. Bei der Registrierung sind alle als notwendig gekennzeichneten Datenfelder vollständig und korrekt auszufüllen.

**2.2** Der Vertrag zwischen GigraWars und dem Nutzer kommt durch die Annahme des Nutzerantrags durch GigraWars zustande. Die Annahme kann entweder ausdrücklich erfolgen oder durch die erste Erfüllungshandlung, insbesondere durch die Bereitstellung des Spiels oder Services.

**2.3** Sofern für die Nutzung eines Spiels oder Services die Erstellung eines Accounts erforderlich ist, wird der Zugang des Nutzerantrags durch GigraWars unverzüglich elektronisch bestätigt und an die vom Nutzer angegebene E-Mail-Adresse versandt. Es ist jedoch zu beachten, dass die Zugangsbestätigung allein keine verbindliche Annahme des Nutzerantrags darstellt. Die Zugangsbestätigung kann jedoch mit der eigentlichen Annahmeerklärung verbunden sein.

## 3 Widerrufsbelehrung und Muster-Widerrufsformular

Widerrufsrecht:  
Der Nutzer hat das Recht, binnen 14 Tagen ohne Angaben von Gründen seine Vertragserklärung zum Abschluss des Spiele- und Servicenutzungsvertrages und zur Bestellung von Premium-Features zu widerrufen.

Die Widerrufsfrist beträgt 14 Tage ab dem Tag des Vertragsabschlusses.

Um das Widerrufsrecht auszuüben, muss der Nutzer GigraWars (DasDomainDepot.de GmbH, Feldstraße 41, 42477 Radevormwald, Deutschland; E-Mail: info@gigrawars.de) mittels einer eindeutigen Erklärung (z.B. ein mit der Post versandter Brief, Telefax oder E-Mail) über seinen Entschluss, seine Vertragserklärung zum Abschluss des Spiele- und Servicenutzungsvertrages bzw. zur Bestellung von Premium-Features zu widerrufen, informieren.

Der Nutzer kann für den Widerruf das unten stehende Muster-Widerrufsformular verwenden, jedoch ist dies nicht verpflichtend. Zur Einhaltung der Widerrufsfrist genügt es, wenn der Nutzer die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absendet. Bei einem Widerruf per E-Mail ist es für eine zügige Bearbeitung hilfreich, in der Betreffzeile den Namen des Spiels, gegebenenfalls der Premium-Features oder des Services, sowie den Namen des Nutzers und die UserID anzugeben.

Folgen des Widerrufs:

Im Falle eines Widerrufs durch den Nutzer verpflichtet sich GigraWars, alle Zahlungen, die GigraWars vom Nutzer erhalten hat, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass der Nutzer eine andere Art der Lieferung als die von GigraWars angebotene, günstigste Standardlieferung gewählt hat), unverzüglich und spätestens innerhalb von 14 Tagen ab dem Tag der Mitteilung über den Widerruf des Nutzers zurückzuzahlen. Für die Rückzahlung wird dasselbe Zahlungsmittel verwendet, das der Nutzer bei der ursprünglichen Transaktion eingesetzt hat, es sei denn, es wurde ausdrücklich etwas anderes vereinbart. In keinem Fall werden dem Nutzer wegen dieser Rückzahlung Entgelte berechnet.

Hinweis:  
Ein Widerruf ist ausgeschlossen, wenn digitale Inhalte geliefert werden, die nicht auf einem physischen Datenträger bereitgestellt werden und die Ausführung mit vorheriger ausdrücklicher Zustimmung des Nutzers und seiner Kenntnisnahme begonnen hat, dass er dadurch sein Widerrufsrecht verliert.

Ende der Widerrufsbelehrung

Muster-Widerrufsformular

(Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden Sie es zurück)

- An DasDomainDepot.de GmbH, Feldstraße 41, 42477 Radevormwald, Deutschland; E-Mail: info@gigrawars.de
- Hiermit widerrufe(n) ich/wir (\*) den von mir/uns (\*) abgeschlossenen Vertrag über den Kauf der folgenden Waren (\*)/ die Erbringung der folgenden Dienstleistung (\*)
- Bestellt am (\*)/erhalten am (\*)
- Name des/der Verbraucher(s)
- Anschrift des/der Verbraucher(s)
- Unterschrift des/der Verbraucher(s) (nur bei Mitteilung auf Papier)
- Datum

(\*) Unzutreffendes streichen.

## 4 Erreichbarkeit
GigraWars gewährleistet eine Erreichbarkeit der Spiele und Services von durchschnittlich 95 % (fünfundneunzig Prozent) im Laufe eines Jahres.

Ausgenommen davon sind Zeiten, in denen die Server der Internetseiten oder der einzelnen Spiele aufgrund von technischen oder anderen Problemen, die nicht im Einflussbereich von GigraWars liegen (höhere Gewalt, Verschulden Dritter usw.), über das Internet nicht erreichbar sind, sowie Zeiten, in denen routinemäßige Wartungsarbeiten durchgeführt werden.

Die Haftung von GigraWars für eine Nichterreichbarkeit der Spiele und Services bei Vorsatz und grober Fahrlässigkeit bleibt unberührt. GigraWars behält sich das Recht vor, den Zugang zu den Leistungen einzuschränken, falls dies für die Sicherheit des Netzbetriebs, die Aufrechterhaltung der Netzintegrität, insbesondere zur Vermeidung schwerwiegender Störungen des Netzes, der Software oder gespeicherter Daten erforderlich ist.

## 5 Zugang und Kenntnisnahme der AGB, Änderungen und weitere Benachrichtigungen, Kontaktaufnahme durch den Nutzer

**5.1** Durch die Absendung des Nutzerantrags und die Nutzung der Spiele oder Services erkennt der Nutzer die AGB an. Die AGB gelten für jegliches Einloggen im Rahmen der Services sowie für jede Teilnahme an den Spielen oder sonstige Nutzung der Services.

**5.2** GigraWars behält sich das Recht vor, die AGB mit Wirkung für die Zukunft jederzeit zu ändern oder zu ergänzen, sofern dies als notwendig erscheint und der Nutzer hierdurch nicht wider Treu und Glauben benachteiligt wird.

**5.3** Änderungen der AGB erfolgen durch Veröffentlichung auf den Internetseiten oder per E-Mail. Der Nutzer wird über Änderungen der AGB beim nächsten Einloggen nach der Änderung durch eine deutlich hervorgehobene Ankündigung informiert. Die aktualisierten AGB treten unverzüglich in Kraft, sobald der Nutzer die Änderungen akzeptiert hat. Die Bestimmungen von Ziff. 5.1 gelten sinngemäß.

**5.4** Wenn der Nutzer die Änderungen der AGB nicht akzeptiert, haben beide Parteien das Recht, den Vertrag mit einer Kündigungsfrist von einem Monat zu beenden, sofern nicht bereits gemäß Ziff. 8.2.1 ein jederzeitiges Kündigungsrecht besteht. Bis zur Beendigung des Vertrags gelten die ursprünglichen AGB weiterhin. Etwaige im Voraus geleistete Leistungsentgelte im Zusammenhang mit zeitlich befristeten Dauerschuldverhältnissen werden dem Nutzer in diesem Fall anteilig zurückerstattet. Weitere Ansprüche des Nutzers sind ausgeschlossen.

**5.5** GigraWars wird in der Mitteilung über die Änderungen auf die Möglichkeit der Nichtanerkennung und der Kündigung hinweisen.

**5.6** GigraWars wird, sofern durch diese AGB oder anderweitige Vereinbarungen mit dem Nutzer nichts anderes bestimmt ist, in der Regel per E-Mail mit dem Nutzer kommunizieren. Der Nutzer stellt sicher, dass er E-Mails, die von GigraWars an die bei der Registrierung angegebene oder später mitgeteilte E-Mail-Adresse versandt werden, empfangen kann. Dies schließt die Anpassung von Spam-Filtereinstellungen und die regelmäßige Überprüfung dieser E-Mail-Adresse ein. GigraWars behält sich jedoch vor, für andere schriftliche Mitteilungen die entsprechende Form der Korrespondenz zu wählen.

**5.7** Bei jeder Kontaktaufnahme mit GigraWars wird der Nutzer angeben, auf welches der Spiele oder Services bzw. auf welchen Spielaccount bzw. Serviceaccount sich sein Anliegen bezieht.

## 6 Spielanleitungen, Spielregeln

**6.1** Die Spielanleitungen und Spielregeln der jeweiligen Spiele und Services werden auf den Internetseiten der Spiele oder Services veröffentlicht.

**6.2** Der Nutzer ist sich bewusst, dass er in den Spielwelten mit zahlreichen anderen Nutzern zusammen spielt und kommuniziert. Um ein harmonisches Zusammenspiel zu gewährleisten, ist die Einhaltung von Regeln notwendig. Mit der Teilnahme erkennt der Nutzer die Spielregeln und Teilnahmevoraussetzungen der Spiele und Services als verbindlich an.

**6.3** Der Nutzer wird darüber hinaus alles unterlassen, was den Betrieb der Spiele und Services sowie das harmonische Zusammenspiel stören könnte.

## 7 Tarifstufen, Zahlungsbedingungen, Verzug

### 7.1 Basis-Version
GigraWars stellt den Nutzern grundsätzlich die Spiele und Services ab dem Zeitpunkt der Account-Erstellung und alternativ nach dem Herunterladen und Installieren zur Verfügung, sofern notwendig. Zu Beginn steht dem Nutzer lediglich eine Basis-Version zur Verfügung. Die Erstellung des Accounts und die Nutzung der Basis-Version sind kostenlos. Die Basis-Version ist zeitlich nicht begrenzt und ermöglicht ein vollständiges Spielerlebnis, allerdings stehen dem Nutzer nicht alle Features zur Verfügung.

### 7.2 Premium-Features
Der Nutzer hat die Möglichkeit, gegen Zahlung eines Entgelts Premium-Features zu erwerben, die in der Basis-Version nicht verfügbar sind. Diese Premium-Features können erweiterte Spielfunktionen oder zusätzliche virtuelle Spielgüter umfassen. Durch den Erwerb von Premium-Features entstehen jedoch weder immaterielle noch materielle Vermögenswerte. Eine Abtretung dieser Features an Dritte ist ebenfalls untersagt.

Die einzelnen Spiele können unterschiedliche Premium-Features anbieten, deren Tarife, Funktionen und Voraussetzungen auf der Internetseite des jeweiligen Spiels aufgeführt sind. Die angebotenen Premium-Features können Einmalzahlungen, die Aufladung eines in dem Spiel nutzbaren Guthabens gemäß dessen Regeln für bestimmte Funktionen oder Zahlungen für einen festgelegten Zeitraum (zum Beispiel Tag, Woche, Monat, Vierteljahr, Halbjahr, Jahr) umfassen. Alle Preise sind Bruttopreise und beinhalten gegebenenfalls anfallende Steuern.

Die Spiele werden kontinuierlich weiterentwickelt, weshalb GigraWars das Recht behält, jederzeit neue Premium-Features anzubieten. Im Zuge der Anpassung und Weiterentwicklung der Spiele kann GigraWars auch entscheiden, einzelne Premium-Features künftig nicht mehr anzubieten oder sie in die kostenlose Basis-Version (vgl. oben 7.1.) zu integrieren. In letzterem Fall sind Ansprüche des Nutzers auf Erstattung der zuvor geleisteten Entgelte ausgeschlossen.

Im Falle der dauerhaften Einstellung eines Spiels gilt Ziff. 1.2.15.

Sofern der Nutzer minderjährig ist, versichert er durch die Bestellung von Premium-Features ausdrücklich, dass ihm die erforderlichen Mittel zur Bezahlung zur Verfügung stehen oder zu diesem Zweck freigegeben wurden.

Falls für bestimmte Spiele ein Zugang über herunterladbare Software für Mobiltelefone möglich ist, gelten für die damit verbundenen Kosten die Regelungen bezüglich Premium-Features.

### 7.3 Abo / Automatische Verlängerung
Wenn Zahlungen für Premium-Features für einen bestimmten Zeitraum zu leisten sind, geht der Nutzer ein Abonnement bzw. ein Dauerschuldverhältnis ein, das sich automatisch verlängert, sofern der Nutzer es nicht gemäß der in Ziff. 8.2 festgelegten Frist vor Ablauf des jeweiligen Zeitraums kündigt. Die genauen Laufzeiten des Abonnements sind den Spielregeln des jeweiligen Spiels zu entnehmen.

### 7.4 Zahlungsbedingungen, Fälligkeit
GigraWars ist berechtigt, für die Nutzung der Premium-Features (siehe oben 7.2) im Voraus Leistungsentgelte zu verlangen. Die Leistungsentgelte werden bei Vertragsabschluss fällig und können durch die vom Nutzer gewählte Zahlungsmethode (wie z.B. Bankkonto, Kreditkarte, Premium-SMS usw.) beglichen werden.

Die technischen Aufzeichnungen, die den Abrechnungen zugrunde liegen, gelten als korrekt, sofern keine Anhaltspunkte für eine Manipulation vorliegen.

### 7.5 Anpassungen der Entgelte
**7.5.1** GigraWars behält sich das Recht vor, die Preise dauerhaft oder zeitlich begrenzt zu senken sowie dauerhaft oder für einen begrenzten Zeitraum neue Produkte, Leistungen oder Abrechnungsmodalitäten anzubieten. Des Weiteren ist GigraWars berechtigt, die Preise für alle Leistungen, die _keine_ Abonnements (Dauerschuldverhältnisse) darstellen, jederzeit mit Wirkung für die Zukunft zu erhöhen.

**7.5.2** Preise im Rahmen von Dauerschuldverhältnissen können mit einer Frist von sechs Wochen nach schriftlicher Vorankündigung oder durch Benachrichtigung per E-Mail an die vom Nutzer angegebene E-Mail-Adresse geändert werden. Der geänderte Preis gilt, wenn der Nutzer nicht innerhalb von sechs Wochen nach der Benachrichtigung dem geänderten Preis widerspricht. Das Vertragsverhältnis wird dann zu den geänderten Konditionen/Preisen fortgesetzt. GigraWars wird in der Benachrichtigung über die Änderungen auf die Möglichkeit des Widerspruchs und der Kündigung, die Frist und die Rechtsfolgen, insbesondere hinsichtlich eines unterbliebenen Widerspruchs, hinweisen. Widerspricht der Nutzer rechtzeitig, haben beide Parteien das Recht, den Vertrag mit einer Frist von einem Monat zu kündigen, sofern nicht bereits nach Ziff. 8.1 ein jederzeitiges Kündigungsrecht besteht. Bis zur Beendigung des Vertragsverhältnisses gelten die ursprünglichen Tarife fort. Etwaige im Voraus über den Beendigungszeitraum vom Nutzer an GigraWars geleistete Leistungsentgelte (Zahlungen) im Zusammenhang mit Dauerschuldverhältnissen werden dem Nutzer anteilig zurückerstattet. Weitere Ansprüche des Nutzers sind ausgeschlossen.

### 7.6 Verzug
Im Verzugsfall ist GigraWars berechtigt, Zinsen in der jeweils gesetzlich vorgesehenen Höhe zu verlangen. GigraWars ist zudem berechtigt, im Falle des Verzugs die Leistungen einzustellen oder die Accounts der Nutzer unverzüglich zu sperren. Während des Zeitraums der Sperrung fallen für etwaige geschlossene Abonnements keine Leistungsentgelte an.

### 7.7 Rückbelastungen, Stornoentgelt
Falls durch das Verschulden des Nutzers oder durch Umstände, die der Nutzer zu vertreten hat, Stornogebühren bei stornierten Transaktionen entstehen, trägt der Nutzer die entstandenen Kosten.

GigraWars ist berechtigt, diese Kosten zusammen mit dem ursprünglichen Entgelt vom Nutzer einzufordern. Zudem steht GigraWars das Recht zu, eine Bearbeitungspauschale in Höhe von bis zu 5,00 Euro pro stornierter Transaktion zu berechnen. Der Nutzer hat jederzeit die Möglichkeit nachzuweisen, dass kein Schaden entstanden ist oder dieser wesentlich niedriger ausfällt.

### 7.8 Keine Gewinngarantie
GigraWars garantiert den Nutzern keine Gewinne. Insbesondere besteht kein Anspruch der Nutzer auf die Auszahlung eines Preises, sofern sich ein solcher Anspruch nicht ausdrücklich aus diesen AGB ergibt.

Ein Anspruch auf Auszahlung eines Gewinns kann nur bestehen, wenn ein solcher Gewinn von GigraWars ausdrücklich ausgelobt wurde.

Ein Anspruch besteht auch dann nicht, wenn GigraWars feststellt, dass der mögliche Gewinnanspruch aufgrund von Manipulationen – sei es technischer, rechtlicher Art oder aufgrund jeglicher strafrechtlich relevanter Manipulationen im Allgemeinen – entstanden sein könnte. Durch die Teilnahme an den Spielen erklärt sich der Nutzer damit einverstanden, dass GigraWars jederzeit eine umfangreiche Untersuchung hinsichtlich der Rechtmäßigkeit eines Gewinnanspruchs verlangen kann. In der Zwischenzeit kann GigraWars ohne gesondertes Einverständnis des Nutzers die Auszahlung des Gewinns verweigern.

Der Gewinnanspruch wird auch dadurch verwirkt, wenn der Nutzer sich entgegen den jeweiligen Spielregeln verhält. In Zweifelsfragen liegt die Beweislast beim Nutzer; dieser hat nachzuweisen, dass er sich in Übereinstimmung mit den Regeln der Spiele verhalten hat. Der Nutzer ist sich der auferlegten Beweislast bewusst und erkennt sie, auch wenn sie möglicherweise nicht gesetzlich vorgeschrieben ist, ausdrücklich an.

### 7.9 Keine Aufrechnung, Zurückbehaltungsrechte
Gegen Forderungen von GigraWars kann der Nutzer nur mit unbestrittenen oder rechtskräftig festgestellten Gegenansprüchen aufrechnen. Ein Zurückbehaltungsrecht kann der Nutzer nur ausüben, wenn sein Gegenanspruch auf demselben Vertragsverhältnis beruht.

## 8 Laufzeit, Kündigung, Sperrung
### 8.1 Laufzeit

Die Verträge zwischen dem Nutzer und GigraWars werden auf unbestimmte Zeit geschlossen, sofern in dem konkreten Angebot von GigraWars nichts anderes bestimmt ist.

### 8.2 Ordentliche Kündigung
Jede Partei ist befugt die Vertragsverhältnisse unter Einhaltung einer Kündigungsfrist, ohne Angabe von Gründen wie folgt zu kündigen:

**8.2.1** Ist für einen Vertrag keine befristete Laufzeit vereinbart worden, steht beiden Parteien ein jederzeitiges ordentliches Kündigungsrecht mit sofortiger Wirkung zu.

**8.2.2** Ist für einen Spiel- oder Servicevertrag bzw. einen Vertrag über die Nutzung/Zurverfügungstellung von Premium-Features (siehe hierzu auch Ziff. 7.2) eine bestimmte Laufzeit („anfängliche Laufzeit”) mit automatischer Verlängerung vereinbart worden, so verlängert sich dieser Vertrag nach Ablauf der anfänglichen Laufzeit, auf unbestimmte Zeit. Dies gilt nicht, soweit der Nutzer oder GigraWars den Vertrag (i) vor Ablauf der anfänglichen Laufzeit mit einer Frist von 7 Tagen (oder ggf. einer kürzeren Frist, sofern vereinbart) oder (ii) nach Verlängerung der anfänglichen Laufzeit jederzeit mit einer Frist von einem Monat (oder ggf. einer kürzeren Frist, sofern vereinbart) kündigt.

### 8.3 Kündigung aus wichtigem Grund
**8.3.1** Das Recht der Parteien zur jederzeitigen Kündigung aus wichtigem Grund bleibt von den vorstehenden Regelungen unberührt.

**8.3.2** Hat GigraWars die außerordentliche Kündigung zu vertreten, werden dem Nutzer die gegebenenfalls von ihm im Voraus für den Beendigungszeitraum geleisteten Entgelte (insbesondere für befristete Premium-Features) anteilig zurückerstattet. Weitere Ansprüche des Nutzers sind ausgeschlossen, sofern nicht anders in diesen AGB vereinbart.

GigraWars ist insbesondere, aber nicht ausschließlich, zur Kündigung aus wichtigem Grund sowie zur dauerhaften Sperrung des Zugangs des Nutzers berechtigt, wenn:
- der Nutzer mit der Zahlung der Entgelte in Höhe von mindestens 5,- € (Euro) in Verzug gerät und trotz zweifacher Mahnung nicht zahlt.
- der Nutzer schuldhaft gegen Gesetze, die Spielregeln und/oder Nutzungsregeln für Services verstößt und trotz Abmahnung den Verstoß nicht einstellt; bei schwerwiegenden Verstößen ist eine Abmahnung entbehrlich, wenn GigraWars ein Festhalten am Vertrag nicht zumutbar ist.
- der Nutzer seinen Account vier Wochen lang und trotz Erinnerung durch GigraWars nicht genutzt hat.

GigraWars ist ein Festhalten am Vertrag in der Regel unter anderem in den folgenden Fällen nicht zuzumuten:
- wenn der Nutzer gegen Strafgesetze verstößt
- wenn der Nutzer gegen das in den Spielregeln einiger GigraWars Spiele festgelegte Verbot von Multiaccounts (siehe oben Ziff. 1.2) verstößt
- wenn der Nutzer gegen das Verbot des Pushing verstößt (siehe oben Ziff. 1.2)
- wenn der Nutzer gegen das Verbot nicht autorisierter Skripte und/oder das Verbot zur systematischen oder automatischen Steuerung der Spiele und/oder das Verbot der Reproduktion, Auswertung oder Veränderung der Spiele, Spielelemente oder der im Rahmen der Services bereitgestellten Inhalte verstößt (siehe oben Ziff. 1.2)
- wenn der Nutzer gegen das Verbot der Ausnutzung von Programmfehlern (Bugs) verstößt (siehe oben Ziff. 1.2)
- wenn der Nutzer gegen das Verbot des Einsatzes von Anonymisierungsdiensten verstößt (siehe oben Ziff. 1.2)
- wenn der Nutzer bei der Registrierung (Registrierformular beim Nutzerantrag, vgl. oben Ziff. 2) oder bei der Zahlung von Premium-Features (vgl. oben Ziff. 7.1) falsche Angaben macht

Im Falle einer von GigraWars berechtigt ausgesprochenen Kündigung aus wichtigem Grund ist GigraWars berechtigt, einen dadurch entstehenden Schaden vom Nutzer ersetzt zu verlangen.

### 8.4 Sperrung
Ungeachtet des Kündigungsrechtes ist GigraWars berechtigt, den Zugang des Nutzers vorübergehend zu sperren, wenn ein Anfangsverdacht des Missbrauchs der Spiele bzw. Services von GigraWars (insbesondere durch Verstoß gegen die Verbote gemäß Ziff. 1.2 und/oder sonstige unerlaubte Manipulationen) und/oder bezüglich sonstiger Verhaltensweisen des Nutzers besteht, die GigraWars zur außerordentlichen Kündigung gemäß Ziff. 8.3 berechtigen würden.

GigraWars trägt keine Gewähr für einen Spielstand und haftet im Falle einer Sperrung nicht für materielle und/oder immaterielle Schäden aufgrund des Verlustes der Teilnahmemöglichkeit für die Dauer der Sperrung.

## 9 Pflichten und andere Obliegenheiten des Nutzers, sowie Haftung des Nutzers für von ihm eingestellte Informationen, Urheberrechte

### 9.1 Pflichten
Die Hauptverpflichtung des Nutzers besteht darin, das vereinbarte Entgelt zu zahlen, sofern er nicht die kostenfreie Basis-Version des Spiels oder der Services in Anspruch nimmt (siehe oben Ziff 7). Darüber hinaus obliegt es dem Nutzer, die geltenden Spielregeln einzuhalten und die korrekten sowie vollständigen Daten bereitzustellen, die GigraWars während des Vertragsabschlusses oder im Verlauf der vertraglichen Beziehung abfragt. Der Nutzer versichert daher, dass alle Angaben, die er im Rahmen des Vertragsangebots, des Vertragsschlusses oder im Verlauf der vertraglichen Beziehungen bezüglich seiner Person und anderer relevanten Umstände (insbesondere Bankverbindung und Kreditkartennummer) macht, vollständig und korrekt sind. Der Nutzer verpflichtet sich, GigraWars unverzüglich über Änderungen der Daten zu informieren und auf Anfrage von GigraWars die Richtigkeit der Daten zu bestätigen. GigraWars behält sich das Recht vor, bei wiederholten Verstößen gegen diese Allgemeinen Geschäftsbedingungen und/oder die jeweiligen Spielregeln trotz Abmahnung oder schwerwiegenden Verstößen gemäß Abschnitt 8.3 die vertraglichen Leistungen und Lieferungen sofort zu sperren und den Vertrag zu kündigen, ohne eine Frist einzuräumen.

### 9.2 Installation von Software
GigraWars übernimmt keine Haftung für Schäden oder Datenverluste, die aufgrund der Installation von Hard- oder Software, die nicht von GigraWars stammt, auf dem Computer, Mobiltelefon oder anderen (mobilen) Endgeräten des Nutzers, die dieser für die Spiele und Services verwendet, entstehen können.

### 9.3 Weitere Obliegenheiten des Nutzers
**9.3.1** GigraWars stellt die Spiele bzw. Services teilweise online zur Verfügung. Die für die Nutzung erforderliche Software auf dem Endgerät des Nutzers, einschließlich des Betriebssystems und Webbrowsers sowie gegebenenfalls erforderlicher Plug-ins, wird weder von GigraWars bereitgestellt noch installiert. GigraWars bietet keine Unterstützung für die Installation dieser Software an. Es liegt in der Verantwortung des Nutzers, sicherzustellen, dass sein Endgerät in einem Zustand ist, der die Nutzung der Spiele und Services von GigraWars ermöglicht. GigraWars gewährt dementsprechend keine technische Unterstützung bei der Installation der lokal erforderlichen Software.

**9.3.2** Der Nutzer verpflichtet sich dazu, sämtliche nicht-öffentlichen Account- und Zugangsdaten (wie Login-Daten, Passwörter, AccountID usw.) streng vertraulich zu behandeln und gegenüber Dritten geheim zu halten. Diese Geheimhaltungsverpflichtung erstreckt sich ebenfalls auf Freunde, Bekannte, Verwandte sowie andere Spieler (z.B. Mitglieder der Allianz) oder sonstige Nutzer der Services. Sollte der Nutzer Kenntnis davon erlangen oder vermuten, dass unbefugten Dritten die Zugangsdaten bekannt sind, verpflichtet er sich, GigraWars umgehend zu informieren, idealerweise über das Support-Kontaktformular zwecks Beweissicherung. Wenn ein Dritter den Account nutzt, nachdem er unbefugt Zugriff auf die Zugangsdaten erhalten hat, weil der Nutzer diese nicht ausreichend vor fremdem Zugriff geschützt hat, muss sich der Nutzer behandeln lassen, als ob er selbst gehandelt hätte. Dies erfolgt aufgrund der geschaffenen Unsicherheit darüber, wer unter dem betreffenden Account agiert hat und wer im Falle von Vertrags- oder anderen Rechtsverletzungen zur Verantwortung gezogen werden kann. GigraWars behält sich das Recht vor, jeden Zugang mit den Daten des Nutzers als Zugang des Nutzers selbst anzusehen. GigraWars empfiehlt aus Sicherheitsgründen regelmäßige Änderungen der Passwörter. Der Nutzer trägt die alleinige Verantwortung für die Nutzung seines Accounts.

**9.3.3** Im Falle eines begründeten Verdachts, dass nicht-öffentliche Account- oder Zugangsdaten unbefugten Dritten bekannt geworden sein könnten, behält sich GigraWars aus Sicherheitsgründen das Recht vor, jedoch ohne Verpflichtung, nach eigenem Ermessen die Zugangsdaten eigenständig und ohne vorherige Ankündigung zu ändern oder die Nutzung des Accounts zu sperren. Der betroffene, berechtigte Nutzer wird unverzüglich über derartige Maßnahmen informiert, und auf Anfrage werden ihm innerhalb angemessener Frist die neuen Zugangsdaten mitgeteilt. Es besteht kein Anspruch des Nutzers darauf, dass die ursprünglichen Zugangsdaten wiederhergestellt werden.

**9.3.4** Der Nutzer ist nicht befugt, weder seinen gesamten Account noch einzelne Spielgegenstände, Premium-Features oder Spielfunktionen an Dritte zu verkaufen oder anderweitig zu übertragen. Ausgenommen hiervon ist der Verkauf oder die Übertragung innerhalb von speziell von GigraWars geschaffenen und bereitgestellten Angeboten zu diesem Zweck. Des Weiteren ist die Weitergabe des Accounts ohne jegliche Form der Gegenleistung explizit erlaubt, wenn der Betreiber darüber in Kenntnis gesetzt wurde.

**9.3.5** GigraWars ergreift Maßnahmen, um seine Systeme vor Virenbefall zu schützen. Dennoch kann eine vollständige Ausschließung eines Virenbefalls nicht garantiert werden. Des Weiteren besteht die Möglichkeit, dass unbefugte Dritte E-Mails unter Verwendung des Namens von GigraWars ohne Zustimmung versenden, die potenziell Viren oder sogenannte Spyware enthalten oder zu Web-Inhalten verlinken, die Viren oder Spyware beinhalten könnten. GigraWars hat darauf keinen Einfluss. Daher ist der Nutzer dazu angehalten, alle eingehenden E-Mails, die angeblich von GigraWars stammen oder in dessen Namen versendet wurden, auf Virenbefall zu überprüfen. Diese Empfehlung gilt ebenso für E-Mails von anderen Nutzern der Spiele oder Services.

**9.3.6** Der Nutzer verpflichtet sich, den Anordnungen von GigraWars und seinen Mitarbeitern sowie Erfüllungs- und Verrichtungsgehilfen zu folgen. Dies schließt besonders die Anweisungen der Administratoren und Moderatoren eines eventuell zugehörigen Forums für das jeweilige Spiel oder Service ein.

**9.3.7** Der Nutzer verpflichtet sich, keinesfalls den Account, den Anmeldenamen oder das Kennwort eines anderen Nutzers zu verwenden.

### 9.4 Pflichten des Nutzers hinsichtlich etwaiger von ihm eingestellten Informationen
**9.4.1** Der Nutzer ist dazu verpflichtet, die Informationen, die er im Rahmen der Services veröffentlicht und anderen Nutzern zur Verfügung stellt, sorgfältig auszuwählen.

**9.4.2** Der Nutzer verpflichtet sich dazu, innerhalb der Services keine Inhalte (wie Bilder, Videos, Links, Namen, Worte) zu verbreiten, die werbende, politische, religiöse, beleidigende, belästigende, gewalttätige, sexistische, pornografische oder anderweitig moralisch bedenkliche oder anstößige, insbesondere rassistische sowie rechts- oder linksextreme Inhalte, Personen oder Darstellungen enthalten. Des Weiteren ist es dem Nutzer untersagt, rechtlich geschützte Begriffe, Namen, Bilder, Videos, Musikstücke, Spiele oder andere geschützte Materialien zu verwenden. Im Falle von Zweifeln hat der Nutzer die von GigraWars beanstandeten Inhalte unverzüglich zu entfernen. GigraWars behält sich ebenfalls das Recht vor, solche Inhalte eigenständig zu entfernen. Der Nutzer verpflichtet sich, sämtliche relevanten Gesetze und Vorschriften, insbesondere in Bezug auf Jugendschutz, Datenschutz, Persönlichkeitsrechte, Schutz vor Beleidigung, Urheberrechte, Markenrechte usw., zu jeder Zeit zu beachten.

**9.4.3** Der Nutzer ist nicht berechtigt, die Services zu illegalen oder unbefugten Zwecken zu nutzen. Insbesondere ist es untersagt, die Benutzernamen oder E-Mail-Adressen anderer Nutzer ohne deren vorherige Zustimmung zu verwenden, um unaufgeforderte E-Mails, Werbebotschaften oder andere gewerbliche oder kommerzielle Mitteilungen zu versenden.

**9.4.4** Bei schuldhafter Zuwiderhandlung gegen die oben genannten Verpflichtungen behält sich GigraWars das Recht vor, die vom Nutzer eingestellten Informationen eigenständig zu löschen.

9.4.5 Insbesondere hat GigraWars das Recht, eingestellte Informationen ganz oder teilweise zu löschen, wenn konkrete Anhaltspunkte für einen Verstoß gegen diese AGB sowie die Anleitungen und Regeln der jeweiligen Services vorliegen oder diese anderweitig rechtswidrig sind. Beispiele hierfür sind Informationen, die:

- offenkundig anstößig, rassistisch, fanatisch oder Gewalt verherrlichend sind;
- eine andere Person belästigen, beleidigen, bedrohen, obszön, diffamierend oder verleumderisch sind;
- sexistisch, pornographisch oder anderweitig jugendgefährdender Natur sind oder einen Link zu einer nicht jugendfreien Website enthalten;
- falsche oder irreführende Informationen enthalten;
- illegale Verhaltensweisen fördern;
- eine illegale oder unberechtigte Kopie oder Verbreitung eines urheberrechtlich geschützten Werks darstellen, beispielsweise durch Bereitstellung von illegalen Computerprogrammen oder Links zu illegalen Computerprogrammen, Informationen zur Umgehung von Kopierschutzvorrichtungen sowie illegalen Musikkopien oder Links zu illegalen Musikkopien oder anderweitigen Verstößen gegen das Urheberrecht;
- das Versenden von "Junk-Mails", "Kettenbriefen" oder unaufgeforderten Massenmails, Sofortnachrichten, "Spimming" oder "Spamming" beinhalten;
- eingeschränkte, nur über ein Kennwort zugängliche oder versteckte Seiten oder Bilder enthalten;
- kriminelle Aktivitäten oder Vorhaben fördern oder Anweisungen zu illegalen Aktivitäten enthalten oder dazu anstiften, insbesondere aber nicht ausschließlich, Informationen enthalten zur Herstellung oder zum Kauf von Waffen, Kinderpornografie, Betrug, Drogenhandel, Glücksspiel, Stalking, Spamming, Spimming, Verbreitung von Computerviren und anderen schädlichen Dateien, Urheberrechtsverletzungen, Patentverletzungen oder Diebstahl von Betriebsgeheimnissen;
- andere Benutzer zur Angabe personenbezogener Daten für kommerzielle oder gesetzwidrige Zwecke oder zur Preisgabe von Login-Daten auffordern;
- kommerzielle Aktivitäten oder Verkäufe beinhalten, wie zum Beispiel Preisausschreiben, Verlosungen, Tauschgeschäfte, Inserate oder Schneeballsysteme;
- eine Abbildung einer anderen Person beinhalten, ohne dass deren Zustimmung vorliegt;

**9.4.6** Ein Anspruch auf Wiederherstellung gelöschter Informationen besteht nicht.

Zusätzlich dazu hat GigraWars das Recht, den Nutzer von der weiteren Teilnahme an den betroffenen Services auszuschließen und im Falle wiederholter Verstöße trotz Abmahnung gegen die oben genannten Vorschriften, den Account des Nutzers fristlos zu kündigen. GigraWars behält sich ausdrücklich das Recht vor, weitere Ansprüche geltend zu machen, insbesondere Schadensersatzansprüche.

**9.4.7** Der Nutzer verpflichtet sich, GigraWars unverzüglich zu informieren, sobald er Kenntnis von einer missbräuchlichen Nutzung der Services durch Dritte oder andere Nutzer erlangt, wie beispielsweise die Verbreitung und Versendung von Inhalten, die gemäß Abschnitt 9.4.5 verboten sind. Zur Gewährleistung einer effektiven Intervention wird der Nutzer gebeten, diese Informationen in Textform (z.B. per E-Mail) mitzuteilen.

### 9.5 Haftung des Nutzers für etwaige von ihm eingestellte Informationen

**9.5.1** Der Nutzer trägt die Verantwortung für Texte, Dateien, Bilder, Fotos, Videos, Sounds, Musikwerke, urheberrechtliche Werke oder andere Materialien, Informationen usw. (im Folgenden als "eingestellte Informationen" bezeichnet), die er im Rahmen der Services bereitstellt oder mit anderen Nutzern austauscht. GigraWars übernimmt weder die Verantwortung für diese Inhalte noch billigt oder unterstützt sie.

**9.5.2** GigraWars hat keine Kontrolle über die im Rahmen der Services von Nutzern eingestellten Informationen. Es erfolgt keine Überprüfung der eingestellten Informationen durch GigraWars vor deren Veröffentlichung. Dennoch ist GigraWars berechtigt, alle eingestellten Informationen auf Verstöße gegen diese AGB und/oder die Spielregeln zu überprüfen und auszuwerten. Sollte GigraWars Kenntnis von rechtswidrigen eingestellten Informationen erlangen oder erhalten, behält sie sich das Recht vor, diese unverzüglich zu löschen.

**9.5.3** GigraWars übernimmt keinerlei Haftung für die eingestellten Inhalte, insbesondere nicht für deren Richtigkeit, Vollständigkeit oder Zuverlässigkeit von Inhalten, Materialien oder Informationen.

### 9.6 Urheberrechte
**9.6.1** Alle Rechte an den vom Nutzer eingestellten Informationen verbleiben beim Nutzer. Durch das Einstellen von Informationen in die Internetseiten und im Rahmen anderer Services gewährt der Nutzer GigraWars eine nicht ausschließliche, unentgeltliche und jederzeit durch Entfernen der Inhalte widerrufliche Lizenz. Diese Lizenz erlaubt es GigraWars, die Inhalte im Rahmen der Services öffentlich zugänglich zu machen und wiederzugeben sowie sie für die Anpassung an die Erfordernisse der Services zu bearbeiten (Format, Größe, etc.), darzubieten, öffentlich anzuzeigen, zu reproduzieren und zu verbreiten.

9.6.2 Zusätzlich hat GigraWars keine Nutzungsrechte an den vom Nutzer eingestellten Informationen. GigraWars ist nicht befugt, die eingestellten Informationen außerhalb der Services zu verbreiten.

9.6.3 Die vom Nutzer im Rahmen der Services eingestellten Informationen können weltweit von Dritten über das Internet eingesehen werden. Der Nutzer erklärt durch die Bereitstellung der Informationen sein Einverständnis hierzu.

9.6.4 Diese Lizenz verliert ihre Gültigkeit, wenn der Nutzer die Inhalte aus den Services löscht.

## 10 Mängelansprüche
**10.1** GigraWars stellt dem Nutzer den Zugang zu den Spielen und Services in ihrer jeweils vorhandenen Fassung zur Verfügung (siehe Abschnitt 1.2). Der Nutzer hat keinen Anspruch darauf, dass ein bestimmter Zustand oder Funktionsumfang des Spiels oder der Services aufrechterhalten oder herbeigeführt wird, einschließlich eines bestimmten Spielstandes. Bei Erwerb von Premium-Features stehen dem Nutzer die gesetzlichen und vertraglich eingeräumten Rechte zu. Der Nutzer ist sich bewusst, dass die Spiele und Services, wie jede Software, nie völlig fehlerfrei sein können. Als mangelhaft gelten die Spiele bzw. Services nur dann, wenn ihre Spielbarkeit bzw. Nutzung schwerwiegend und nachhaltig beeinträchtigt ist.

Der Nutzer ist dazu verpflichtet, auftretende Mängel an den Spielen, Services oder sonstigen Leistungen oder Lieferungen von GigraWars stets aussagekräftig zu dokumentieren und insbesondere unter Protokollierung angezeigter Fehlermeldungen in Textform zu melden. Vor der Meldung eines möglichen Fehlers sollte der Nutzer die Spiel- bzw. Serviceanleitung sowie gegebenenfalls andere von GigraWars bereitgestellte Hilfestellungen zur Problembeseitigung (insbesondere Listen mit häufig gestellten Fragen und Diskussionsforen zu Problemen) konsultieren. Der Nutzer hat GigraWars nach besten Kräften bei einer möglichen Mängelbeseitigung zu unterstützen.

**10.2** Der Nutzer ist verpflichtet, Mängel unverzüglich nach deren Entdeckung gegenüber GigraWars zu rügen. Zu Beweissicherungszwecken wird dem Nutzer empfohlen, sämtliche Mängelrügen in Textform (per Fax, Brief oder über das Support-Kontaktformular) an GigraWars zu richten.

**10.3** Von der Gewährleistung ausgeschlossen sind grundsätzlich Fehler, die durch äußere Einflüsse, vom Nutzer zu vertretende Bedienungsfehler, höhere Gewalt oder nicht von GigraWars durchgeführte Änderungen oder sonstige Manipulationen entstehen.

**10.4** GigraWars übernimmt keine Garantien im rechtlichen Sinne.

# Haftung und Haftungsbeschränkung
**11.1** Der Nutzer haftet persönlich und unmittelbar für die von ihm zu vertretende Verletzung von Rechten Dritter. Er verpflichtet sich, GigraWars sämtliche Schäden zu ersetzen, die aus der Nichtbeachtung der in diesen AGB festgelegten Pflichten entstehen. Der Nutzer entbindet GigraWars von sämtlichen Ansprüchen, die von anderen Nutzern oder sonstigen Dritten wegen Verletzung ihrer Rechte durch vom Nutzer eingestellte Inhalte oder aufgrund der Verletzung anderer Pflichten gegenüber GigraWars geltend gemacht werden. Dabei trägt der Nutzer die Kosten der erforderlichen Rechtsverteidigung von GigraWars, einschließlich aller Gerichts- und Anwaltskosten. Diese Regelung findet keine Anwendung, wenn die Rechtsverletzung nicht vom Nutzer zu vertreten ist.

**11.2** In Fällen, in denen GigraWars Leistungen unentgeltlich erbringt, haftet GigraWars in keinem Fall für Schäden, die nicht grob fahrlässig oder vorsätzlich verursacht wurden. Diese Regelung zum Haftungsausschluss gilt jedoch nicht für Fälle, in denen Leben, Körper und Gesundheit verletzt werden oder GigraWars eine ausdrückliche Garantie übernommen hat.

**11.3** Wenn GigraWars für Leistungen ein Entgelt verlangt, haftet GigraWars bei Vorsatz und grober Fahrlässigkeit uneingeschränkt. Bei leichter Fahrlässigkeit haftet GigraWars nur im Falle der Verletzung wesentlicher Vertragspflichten oder der Verletzung einer Garantie. Wesentliche Vertragspflichten sind solche Pflichten, die die ordnungsgemäße Durchführung des Vertrages erst ermöglichen und auf deren Erfüllung der Nutzer vertrauen darf. Die oben genannten Haftungsbeschränkungen gelten nicht für die Haftung bei Verletzung von Leben, Körper und Gesundheit oder im Falle der Übernahme einer Garantie durch GigraWars. Die Haftung von GigraWars nach dem Produkthaftungsgesetz bleibt unberührt.

**11.4** Bei der Verletzung wesentlicher Vertragspflichten ist die Ersatzpflicht jeweils auf den vorhersehbaren Schaden beschränkt.

**11.5** Die oben genannten Haftungsausschlüsse bzw. -beschränkungen gelten auch in Bezug auf die Haftung der Angestellten, Arbeitnehmer, Mitarbeiter, Vertreter und Erfüllungsgehilfen von GigraWars, insbesondere zugunsten der Anteilseigner, Mitarbeiter, Vertreter, Organe und deren Mitglieder in Bezug auf ihre persönliche Haftung.

**11.6** GigraWars haftet für Beratung nur in dem Umfang, in dem die Fragestellung den Inhalt des Angebots betroffen hat.

**11.7** GigraWars distanziert sich ausdrücklich von den Inhalten sämtlicher Seiten, auf die direkte oder indirekte Verweise, sogenannte "Links", aus dem Angebot der GigraWars bestehen. GigraWars übernimmt für diese Inhalte und Seiten keinerlei Haftung. Verantwortlich für die Inhalte dieser Seiten sind die Anbieter der jeweiligen Seiten selbst.

## 12 Schlussbestimmungen
**12.1** Anzeigen und Erklärungen, die der Nutzer gegenüber GigraWars abzugeben hat, müssen in Textform erfolgen. Dies gilt auch für die Abänderung oder Aufhebung des Textformerfordernisses.

**12.2** Gerichtsstand ist Köln, Deutschland.

**12.3** Für (i) diese AGB, inklusive zukünftige Abänderungen sowie (ii) für die auf der Grundlage dieser AGB abgeschlossenen Verträge und (iii) für daraus folgende Ansprüche gleich welcher Art gilt ausschließlich das Recht der Bundesrepublik Deutschland unter Ausschluss der Bestimmungen zum einheitlichen UN-Kaufrecht über den Kauf beweglicher Sachen und unter Ausschluss des deutschen internationalen Privatrechts.

**12.4** Sollten Bestimmungen dieser AGB unwirksam sein oder werden, so berührt dies die Wirksamkeit der übrigen Bestimmungen nicht.

Radevormwald, den 23.03.2024
