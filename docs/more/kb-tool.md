# KB-Tool

Das KB-Tool (Kampfberichte Tool) bietet die Möglichkeit Kampfberichte mit anderen Spielern zu teilen, ohne dabei zu viele
Informationen über einen selbst preiszugeben.

Im Bericht werden nur die Verluste beider Parteien sowie deren Allianzen angezeigt. Optional können noch die erbeuteten
Rohstoffe mit angezeigt werden. Alle weiteren Informationen wie Namen, Koordinaten und die ursprüngliche Flotte des
Gewinners bleiben im Verborgenen.

## Bericht hochladen

Kampfberichte können auf der [Startseite](https://www.gigrawars.de/battlereports) hochgeladen werden. Dazu wird
lediglich der Link zum Kampfbericht benötigt. Dieser sieht in etwa wie folgt aus:

```
https://uni4.gigrawars.de/kb_index/id/xxxxxxx
```

Beim Hochladen kann auch entschieden werden, ob die Rohstoffe des Berichtes mit veröffentlicht werden. Genauso kann
entschieden werden, ob der Bericht in der Statistik der Startseite mit aufgeführt werden soll. 
