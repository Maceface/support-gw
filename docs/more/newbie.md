# Neulingsschutz

Willkommen bei der Neulingshilfe, hier findest du allgemeine Informationen zum Neulingsschutz.

## Bedeutung

Der Neulingsschutz soll neuen Spielern den späteren Einstieg ins Spiel etwas erleichtern, dabei geht es primär darum sie
erst einmal in einer abgesicherten Umgebung groß werden zu lassen.

Quasi sind die Neulinge, was Kampfhandlungen angeht unter sich und können sich nur gegenseitig angreifen oder
ausspionieren.

## Funktionsweise

Die Funktion ist bewusst sehr einfach gehalten. Jeder Account hat ein bestimmtes Level, dieses Level wird aus allen
bisher ausgegebenen Rohstoffen sowie dem Registrierungsdatum berechnet.

Anhand des Levels wird entschieden, ob man sich im Neulingsschutz befindet oder außerhalb.

## Level Berechnung

Wir nehmen die 4 Rohstoffe und schauen wie viel du davon in Gebäude, Forschungen, Schiffe und Verteidigungsanlagen
investiert hast und rechnen diese in Punkte um.

Gleiches gilt für das Alter deines Accounts.

| Rohstoff          | Punkte |
|-------------------|--------|
| Eisen             | 10     |
| Lutinum           | 12     |
| Wasser            | 2      |
| Wasserstoff       | 15     |
| Tage ab Anmeldung | 9.000  |

## Erkennung anderer Spieler

In der Galaxieansicht deines Accounts findest du in der Legende den Punkt "nicht angreifbar", dieser zeigt dir alle
entsprechenden Spieler, die durch den Neulingsschutz außerhalb deiner Reichweite liegen.

Das heißt, entweder bist du gerade im Neulingsschutz, dann sind alle außerhalb als nicht angreifbar markiert oder du
befindest dich aber selbst außerhalb des Schutzes, so werden alle im Schutz als nicht angreifbar markiert.

## Mein Level

Du kannst dein aktuelles Level im Menüpunkt Statistik > Rohstoffe sehen.

## Neulingsgrenze / Level

Das Level, ab welchem man keinen Neulingsschutz mehr genießt, ist im Spiel selbst nicht einsehbar.

## Inaktive Spieler

Jeder Spieler, der sich seit mindestens 30 Tagen nicht ins Spiel eingeloggt hat (Ausnahme bildet hier der Urlaubsmodus)
kann von allen Spielern angeflogen werden. Vor dem Ablauf der 30 Tage allerdings nur von den Neulingen.

Für weitere Fragen melde dich gerne auf unserem [Discord](discord.md), dort findest du auch unsere
Neulingshelfer.
