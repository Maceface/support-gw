# FAQ

## Was bedeutet CKK

Als Cougar-Kampfkraft, kurz CKK, wird eine Kennzahl bezeichnet mit der die Angriffs- und Verteidigungswerte der
Einheiten ins
Verhältnis gesetzt werden sollen.
Den Basiswert bildet hier der Cougar mit einem CKK Wert von 1. In den CKK wird immer nur der Basiswert ohne Forschungen
berücksichtigt.

## Allianzmanager

Unter Allianzmanager, kurz AM, versteht man eine externe Software. Mehrere Allianzen haben sich selbst oder auf basis
des [YAAM](https://github.com/bmarwell/yaam) (Yet another alliance manager) einen Allianzmanager aufgebaut. In diesem
pflegen die Spieler ihren Planetenausbau, sammeln Kampfberichte, analysieren mögliche inaktive Spieler oder sammel ihre
Kampfberichte um gegnerische Flotten zu erkennen. Über die Jahre gab es immer wieder Diskussionen den AMs mehr zu
erlauben oder diese zu verbieten.

## Angriffssperre

In besonderen Situation wie beispielsweise Wartungsarbeiten oder auch bei ungeplanten Serverausfällen, kann es darauf
hin zu einer Angriffssperre kommen.
Im Normalfall läuft eine Angriffssperre so ab, dass Flotten mit dem Befehl Erkunden oder Angriff, die sich noch im Flug
befinden, am Ziel keinen Kampf austragen, sondern einfach zum Startplaneten zurückkehren. Außerdem können in der Zeit
keine Erkundungen oder Angriffe gestartet werden.
Sollte eine Flotte vor der Angriffssperre gestartet worden sein und erst danach am Ziel ankommen, dann ist diese Flotte
nicht betroffen.

Während der Angriffssperre können weiter alle anderen Flottenbefehle genutzt werden.

## Weihnachtsplaneten

Um in der dunkeln Jahreszeit ein wenig Abwechslung ins Spiel zu bringen, gibt es vom 1. bis einschließlich den 24.
Dezember in jedem Sonnensystem einen Weihnachtsplaneten.
Dieser kann mit dem Befehl Angriff täglich angeflogen werden, wofür es entsprechende Überraschungen gibt.

## Intergalaktischer Briefträger

Man munkelt, dass irgendwo ein etwas verwirrter Briefträger Requisiten verteilt. Verrückt.
