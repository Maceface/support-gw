# Unterstützen

Du möchtest GigraWars unterstützen? Da gibt es viele Optionen.

## Spiel bis der Arzt kommt

Die wichtigste Option ist natürlich spielen. Es klingt oft sehr scheinheilig, aber das Spiel lebt von den Spielern,
die es spielen. Wir betreiben dieses kleine Projekt, weil es uns Spaß macht, ohne Spieler wäre es nur wenig Spaß.

## Trage deine Begeisterung weiter

Kennst du andere Spieler die unbedingt GigraWars kennenlernen müssen? Dann überzeuge sie von GigraWars.
Neue Spieler bringen mehr Abwechslung und im Zweifel mehr Farmpotential.

Vielleicht bist du ja auch in anderen Communities unterwegs wo es in Ordnung ist ein wenig von GigraWars zu erzählen.

## Voting

- [mmofacts.com](https://de.mmofacts.com/gigrawars-7275) (Mit Account bewerten oder auf Spielen klicken)
- [pewn.de](https://pewn.de/games/349111-GigraWars/) (Mit Account bewerten oder unten rechts auf Web klicken)
- [gamessphere.de](https://gamessphere.de/games/gigrawars/) (Mit Account bewerten oder unten auf Voten klicken)
- [browsergame-magazin.de](http://www.browsergame-magazin.de/gigrawars/) (Bewerten oder unten einen Kommentar
  hinterlassen)

## Werbung

Im Spiel werden Werbebanner angezeigt, mit welchen wir eine Kleinigkeit nebenher verdienen, um die laufenden Kosten zu
decken. Aus diesem Grund wäre es nett, wenn du deinen Adblocker für uns deaktivieren würdest. Siehst du spannende
Werbung, dann klick diese gerne an und besuche die entsprechende Partnerseite.

## Trinkgeld

Du bist der Meinung, dass du uns etwas zurückgeben willst für die investierte Zeit? Da würden wir uns natürlich sehr
freuen.
[paypal.me/gigrawars](https://www.paypal.me/gigrawars)

## Mithelfen

Es gibt viele Möglichkeiten wie du uns weiterhelfen kannst. Meld dich gerne bei uns, hast du Beispielsweise
Verbesserungsvorschläge für die Hilfe?
Dann kannst du gerne bei [Gitlab.com/gigrawars-open/info](https://gitlab.com/gigrawars-open/info) einen Merge Request
für das Projekt erstellen.

Weißt du nicht was mit Merge Request gemeint ist? Kein Problem, dann lass uns deine Vorschläge gerne so via Discord
zukommen und wir pflegen sie entsprechend ein.
