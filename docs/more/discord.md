# Discord

Auf unserem Discord Server bekommst du alle Neuigkeiten rund um das Spiel mit und kannst jederzeit Fragen stellen.
Unsere Community kann dir sicher einige Fragen beantworten oder auch Tipps geben.

[Jetzt beitreten](https://www.gigrawars.de/discord)

## Neulingshelfer

Auf unserem Discord im Channel _#neulingsfragen_ kannst du alle Fragen zum Spiel stellen die dir einfallen, außerdem
findest du dort Neulingshelfer die dich mit Tipps und Rohstoffen versorgen.

## Software

Discord ist ein kostenloses Chat- und Sprach-Programm, um beispielsweise innerhalb der Allianz zu kommunizieren. Discord
ist kostenlos verfügbar und es gibt zwar eine kostenpflichtige Erweiterung (<https://discord.com/nitro>), allerdings
wird diese nicht benötigt.

::: tip Discord Server Boosts

Solltest du Nitro haben, kannst du gerne unseren Discord Server boosten, damit wir coole Features für die Community
freischalten können.
:::

## Account

Der Discord Account ist komplett unabhängig vom GigraWars Account, das heißt du musst einen Account anlegen falls du
nicht bereits Discord Nutzer bist.

Zur Anmeldung: <https://discord.com/register>

Der Benutzername und die Mailadresse müssen nicht zwingend mit den Daten in GigraWars übereinstimmen, da wie bereits
erwähnt der Account unabhängig ist. Sobald du dich später auf dem Discord Server von GigraWars angemeldet hast, kannst
du dort noch einen anderen Namen wählen. Wir überlassen es euch wie ihr euch auf dem GigraWars Discord benennt. Ein paar
der Spieler haben sich auf "\[Allianz-Tag\] Spielername" geeinigt, dies ist aktuell allerdings keinesfalls
verpflichtend.

Nach der erfolgreichen Anmeldung musst du noch deine Mailadresse bestätigen, wir lassen auf unserem Server nur Nutzer zu
die min. eine verifizierte Mailadresse haben um Spam vorzubeugen.

Jetzt kannst du unserem Discord Server beitreten, dazu musst du lediglich auf den folgenden Link klicken.

<https://www.gigrawars.de/discord>

## Formatierungen

Discord bietet verschiedene Möglichkeiten, um seine Nachrichten zu formatieren, zum einen gibt es da die Markdown
ähnlichen Ansätze.

### Standard

| Beispiel             | Code                       |
|----------------------|----------------------------|
| *kursiv*             | `*kursiv*` oder `_kursiv_` |
| **Fett**             | `**fett**`                 |
| _**Fett Kursiv**_    | `***fett kursiv***`        |
| <u>Unterstrichen</u> | `__unterstrichen__`        |

### Weiteres

Neben den Standardformatierungen bietet Discord auch Code-Blöcke, Zitate und Spoiler Tags an. Alle Informationen dazu
findest du in
der [Discord Hilfe](https://support.discord.com/hc/de/articles/210298617-Markdown-Text-101-Chat-Formatierung-Fett-Kursiv-Unterstrichen-#)
.

## 2 Faktor Auth

In der heutigen Zeit ist es immer wichtiger seine Accounts mit der sogenannten 2 Faktor Authentifizierung zu schützen.
Einfach gesagt, geht es darum einen zweiten Faktor mit einzubringen, um mehr Sicherheit zu haben. Für den Fall, dass
jemand euer Passwort herausfindet, benötigt er oder sie noch zusätzlich Zugriff auf euren zweiten Faktor. Dieser zweite
Faktor ist im Falle von Discord eine App auf deinem Smartphone. Dabei habt ihr die Wahl zwischen Authys und dem Google
Authenticator.

Wir empfehlen die App von Google meist, da die Verbreitung größer ist, allerdings sind beide Apps gleich gut und beide
Empfehlenswert.
