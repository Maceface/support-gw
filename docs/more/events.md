# Events

Events sind besondere Ereignisse, welche von Spielern für Spieler oder von der Spielleitung für Spieler organisiert
werden.
In jedem Fall gilt es Events frühzeitig bei der Spielleitung anzumelden. Zum einen um der Spielleitung die Möglichkeit
zu geben ihre Unterstützung anzubieten und zum anderen um mögliche Überschneidungen von Events zu verhindern.

## Hintergrund

Events bieten die Möglichkeit abseits vom gewohnten Spiel einen Anreiz zu bieten. Außergewöhnliche Konzepte oder auch
reines Kräftemessen sind möglich. Manchmal bieten Events auch einen Mehrwert durch Rohstoffgewinne oder Sachpreise.

## Beispiele

### X-mas Bash Event

Im Dezember 2020 fand von Spielern für Spieler ein Bash Event statt, dieses lief 10 Tage, mit einer kurzen Pause über
die Weihnachtsfeiertage.
Angetreten sind dort Allianzen gegeneinander, Awards gab es für

- meiste zerstörte Schiffe (ohne Sonden)
- meiste zerstörte Sonden
- meiste recycleten Ressourcen
- meiste geplünderte Ressourcen (ohne Wasser)
- meiste produzierte KBs

Am Event nahmen 11 Allianzen teil.

### Northcup

Im Frühjahr 2021 fand der erste Northcup, als ein Event der Spielleitung, statt. Das Event lief ganze 3 Wochen und
versuchte aus den Gewohnheiten des Spiels ausbrechen.
Der Fokus lag darauf, Angriffe oder Deffs mit Recyclern zu provozieren, da nur diese zu Punkten führten.

Auszug aus der Wertung:
> Jeder zerstörte Recycler gibt einen Punkt, dabei gilt allerdings die geringere Menge an Recyclern, welche auf einer
> Seite in den Kampf verwickelt waren. Ob die Recycler des Gewinners dabei zerstört worden ist für die Wertung nicht
> relevant. Die Punkte bekommt immer nur der Gewinner des Kampfes gutgeschrieben. Gewertet werden nur Kämpfe zwischen
> teilnehmenden Allianzen.

> Beispiele  
> Gewinner hatte 100 Recycler; Verlierer hatte 10 Recycler; 10 Punkte für den Gewinner  
> Gewinner hatte 1 Recycler; Verlierer hatte 10 Recycler; 1 Punkt für den Gewinner  
> Gewinner hatte 1 Recycler; Verlierer hatte 0 Recycler; 0 Punkte für den Gewinner

### GigraWars Weltmeisterschaft 2021

Im Sommer 2021 fand die GigraWars Weltmeisterschaft 2021, kurz GWM-21, statt. Ins Leben gerufen von Spielern für Spieler
und unterstützt durch die Spielleitung.

Bei diesem Event war die Grundidee, eine Art Turnier abzubilden, ähnlich zur laufenden Fußballmeisterschafft.

Teilgenommen haben 13 Teams, welche sich passend zum Fußball Thema entsprechende Teamnamen gaben:

-
    1. Vampire S.W.A.T.
- Borussia PSC
- Chinesische Nationalmannschaft
- CRa7y
- Fortuna Fox United
- Krückstock-Altherren
- Nordkoreanische Nationalmannschaft
- Planet Kickers United
- Saudi-Arabische Nationalmannschaft
- SC Jedi
- Serious United
- SV Riskante Blume 21
- Trümmerfeld FC

Insgesamt gab es 23 "Spiele", wobei jedes Spiel von Montags bis Freitags ging, plus eine mögliche Verlängerung.
