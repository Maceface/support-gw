# Externe Seiten

## Tools von nicolausocin

Auf der Seite [gigra.nicolausocin.de](http://gigra.nicolausocin.de/) finden sich mehrere Tools, welche eine nützliche
Ergänzung zum Spiel bieten.

- CKK-Rechner
- Fakeatrechner
- Punktetabellen
