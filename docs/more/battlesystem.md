# Kampfsystem

## Recycling

Am Ende des Kampfes kommen die Recycler des Siegers zum Einsatz, dabei berechnet sich die Menge an Rohstoffen anhand der
zerstörten Schiffe, sowie der [Recycling-Technik](../help/research.md#recycling-technik) Forschungsstufe.

Zusätzlich gibt es bei der Ladekapazität der Recycler einen Unterschied zwischen Angriff und Verteidigung. Wird ein
Kampf auf einem Heimatplaneten gewonnen, gilt die größe der dortigen Rohstoffspeicher. Es muss lediglich ein Recycler
den Kampf überleben.

Bei einem Kampf auf einem fremden Planeten ist hingegen die Ladekapazität der Recycler ausschlaggebend.
