# Highscore

Die Highscore in Gigrawars ist ein Maß für den Erfolg und die Leistung von Spielern und Allianzen hinsichtlich des
Account Ausbaus. Es gibt ein Spieler-Ranking, Allianz-Ranking und ein Ranking der Planeten. Die Punkte setzen sich aus
Planetenpunkten und Forschungspunkten zusammen. 

Die Highscore lässt jedoch keine Rückschlüsse auf die Flottenstärke der Spieler zu.
