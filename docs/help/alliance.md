# Allianz

## Gestaltung
Für die Gestaltung der Allianzbeschreibung ist mit sogenannten BB-Codes möglich.

### Einfache Formatierungen
```
[b]Fettgedruckter Text[/b]
[i]Kursiver Text[/i]
[u]Unterstrichener Text[/U]
```

### Textgröße
Der BB-Code `[size]` funktioniert mit den Zahlenwerten von 4 bis 18.
```
[size=4]Kleinste Textgröße[/size]
...
[size=18]Größte Textgröße[/size]
```

### Ausrichtung
```
[align=left]Linkbündiger Text[/align]
[align=center]Zentrierter Text[/align]
[align=left]Rechtsbündiger Text[/align]
```

### Textfarbe
Der BB-Code für die Textfarbe erlaubt HEX Codes oder Farbnamen. [Farbtabelle](https://www.farb-tabelle.de/de/farbtabelle.htm)

```
[color=green]Grüner Text[/color]
[color=#FF0000]Roter Text[/color]
[color=#000]Schwarzer Text[/color]
```

### Links

``` 
[url]https://www.gigrawars.de[/url]
[url=https://www.gigrawars.de]GigraWars.de[/url]
```

### Bilder
Es sind Bilder mit den Endungen .jpg oder .png sowie .gif möglich.
```
[img]https://www.example.com/image.jpg[/img]
```

### Hintergrundbild
```
[background=https://www.example.com/backgroundimage.jpg]Text auf dem Hintergrundbild[/background]
```
