# Statistiken

In den Statistiken befinden sich spannende Informationen über den Account.

## Kampfbilanz

Hier findest du die Bilanz aller Kämpfe in deinem Account.

### Kampfbilanz Schiffe / Verteidigungen

Die Kampfbilanz gibt einen Überblick über die verloren und zerstörten Schiffe und Türme in deinem Account.

### Rohstoffbilanz Angriffe

Die Rohstoffbilanz Angriffe gibt einen Überblick über die verloren und erfarmten Rohstoffe in deinem Account.

### Rohstoffbilanz Recycling

Die Rohstoffbilanz Recycling gibt einen Überblick über die gewonnenen Rohstoffe durch Recyclen (Att oder Deff).

## Rohstoffe

Unter dem Reiter Rohstoffe lassen sich die Summe aller Rohstoffe im Account einsehen, sowie die Rohstoffe, die sich auf
jedem einzelnen Planeten befinden bzw. in der Schiffsproduktion (Bauschleife) des jeweiligen Planeten.
