# Targetlisten

Die Targetlisten bieten die Möglichkeit einen Überblick über Ziele zu behalten, welche häufig angeflogen werden soll.
Es können Beispielsweise die Farmlisten oder auch die Koordinaten der Kriegsgegner gepflegt werden.

Jeder Planet hat seine eigene Targetliste auf welcher mit den Rängen 1 bis 9 entsprechende Einträge gepflegt werden
können.

## Export

Über die Exportfunktion können die Targetlisten exportiert werden, dabei werden immer *alle* Listen exportiert.
Es handelt sich dabei um das [XML](https://de.wikipedia.org/wiki/Extensible_Markup_Language) Format.

Nachfolgend ein Beispiel für einen Export:

```xml
<targetlists>
    <targetlist planet="1:1:1">
        <target sortorder="1" comment="">2:2:2</target>
        <target sortorder="1" comment="">1:1:1</target>
    </targetlist>
    <targetlist planet="2:1:1">
        <target sortorder="1" comment="">1:1:1</target>
        <target sortorder="5" comment="">2:3:1</target>
    </targetlist>
    <targetlist planet="2:1:2"/>
</targetlists>
```

Der Export der Targetlisten wird immer von einem `<targetlists>` sowie einem `</targetlists>` eingefasst.

Bei den Targetlisten selbst gibt es dann zwei möglichkeiten. Ist eine Targetliste leer (ohne Elemente), dann
sieht sie aus wie der Planet 2:1:2

```xml
<targetlist planet="2:1:2"/>
```

Hat eine Targetliste jedoch Einträge, so besteht auch diese aus einem `<targetlist planet="1:1:1">`, sowie
einem ` </targetlist>`.
Hierbei steht die 1:1:1 für die Koordinate zu welcher die Targetliste gehört.

Die Elemente der Targetliste sehen dann immer wie folgt aus:

```xml
<target sortorder="1" comment="Kommentar">2:2:2</target>
```

Wie hier zu sehen beinhaltet die Zeile den *Rang* aus dem Interface im Spiel, hier allerdings als `sortorder="1"`
gespeichert.
Direkt dahinter folgt der optionale Kommentar im Beispiel mit `comment="Kommentar"`. Zum Schluss noch die Koordinate
selbst, auch als Target bekannt.

## Import

Wichtig beim Import ist, dass *alle* Targetlisten von Planeten überschrieben werden, die im Import vorhanden sind.

Um dem Beispiel des Exports zu folgen würde man mit folgendem Import nur die Targetliste von Planeten 1:1:1
überschreiben.

```xml
<targetlists>
    <targetlist planet="1:1:1">
        <target sortorder="1" comment="">3:1:1</target>
        <target sortorder="1" comment="">3:1:2</target>
    </targetlist>
</targetlists>
```

Wohingegen man mit dem folgenden Beispiel die Targetliste von Planet 2:1:1 komplett leeren würde.

```xml
<targetlist>
    <targetlist planet="2:1:1"/>
</targetlists>
```

::: tip Dynamische Koordinate

Es kann statt der Koordinate des Planeten das Schlüsselwort "current" genutzt werden, dadurch wird die Liste für den aktuellen Planeten importiert.
:::
