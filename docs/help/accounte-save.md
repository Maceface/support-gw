# Account sichern

Eins der wichtigsten Themen bei GigraWars ist das Sichern des Accounts beziehungsweise der Rohstoffe und Schiffe.

## Ich werde angegriffen!

Angriffe gehören dazu bei GigraWars, es ist immerhin ein Teil des Namens, nur besteht die Kunst darin seinem Gegner
nichts von seinen Rohstoffen zu schenken und am besten auch keine Schiffe zu verlieren. Um das zu schaffen, bietet
GigraWars verschiedene Möglichkeiten seine Rohstoffe zu “verstecken”.

Erst einmal solltest du in deine Rohstoffübersicht schauen, dort findest du die Plündergrenze deiner Lager, sind deine
Rohstoffe unter diesem Wert, so kann sie dir auch niemand wegnehmen. Solltest du allerdings mehr Rohstoffe haben, gibt
es ein paar Optionen, wie du deine Rohstoffe sichern kannst, damit sie niemand bekommt.

## Flotte sichern

Hast du zufällig eine Flotte auf dem Planeten stehen, mit genug Ladekapazität, dann könntest du deine Schiffe mit den
Rohstoffen auf einen unbesiedelten Planeten schicken, mit dem Befehl Transport. In der Zeit, in der deine Flotte mit den
Rohstoffen unterwegs sind, kommt niemand an die Rohstoffe und natürlich auch nicht an deine Schiffe.

::: tip Nicht besiedelbare Planeten

Fliege immer auf die Planeten der Koordinate 20 bis 99, diese werden zwar in der Galaxie nicht angezeigt, weil sie nicht
besiedelt werden können, aber trotzdem kannst du Schiffe dort hinschicken.
:::

Des Weiteren empfiehlt es sich immer, die Schiffe nicht sehr weit zu schicken, sondern lieber die Geschwindigkeit zu
reduzieren, damit sparst du gerade bei großen Flotten sehr viel Wasserstoff.

## Bauschleife sichern

Solltest du keine Flotte auf deinem Planeten haben oder hat deine Flotte nicht genug Ladekapazität, so gibt es noch
weitere Möglichkeiten, deine Rohstoffe zu sichern.

Vielleicht ist dir schon aufgefallen, dass Schiffe und Verteidigungsanlagen, die du in Auftrag gibst, direkt von deinem
Rohstoffkonto abgezogen werden. Zusätzlich werden dir die gesamten Baukosten erstattet, wenn du Bauaufträge wieder
abbrichst. Das eröffnet dir eine sehr effektive Möglichkeit, deine Rohstoffe zu sichern. Du kannst unter anderem ein
Schiff mit einer sehr langen Bauzeit an den Anfang der Bauschleife setzen und dahinter mit bestimmten Schiffen versuchen
alles an Rohstoffen zu sichern. Immer wenn jetzt das erste Schiff fast fertig wird, brichst du die Bauaufträge einfach
wieder ab.

Achte hier aber darauf, dass wenn du eine sehr lange Bauschleife hast und deine Speicher nicht sonderlich groß sind,
dass du dann möglicherweise Rohstoffe verlierst.
