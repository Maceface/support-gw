# Galaxie

Über die Galaxieansicht besteht die Möglichkeit sich einen Überblick über das gesamte Universum zu verschaffen.

## Aufbau der Galaxie

Für ein besseres räumliches Verständnis ist es wichtig zu verstehen wie das Universum aufgebaut ist.
Man kann sich das Universum wie einen langen Streifen vorstellen, wo alle Galaxien nebeneinander aufgereiht sind und
entsprechend ihre Systeme darin.

Daraus folgt, wenn die Galaxie 4 100 Sonnensysteme hat, dass das letzte Sonnensystem direkt neben dem ersten
Sonnensystem der Galaxie 5 liegt.

## Farblegende

| #                | Farbe        |
|------------------|--------------|
| Eigene Planeten  | Dunkles Blau |
| Urlaubsmodus     | Orange       |
| Eigene Allianz   | Helles Blau  |
| Administrator    | Rot          |
| Nicht Angreifbar | Helles Braun |
