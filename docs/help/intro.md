# Willkommen

Willkommen im Herzstück der GigraWars Hilfe.

In der Navigation findest du erst ein paar Einsteiger Tutorials. Die weiteren Erklärungen sind thematisch sortiert.
Am Schluss befinden sich noch weitere Erklärung für externe Systeme.

## Community
Auf unserem [Discord](../more/discord.md) findest du Gleichgesinnte und unsere Neulingshelfer. Scheue nicht Fragen zu stellen.
