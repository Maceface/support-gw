module.exports = {
    theme: "vt",
    title: 'GigraWars Info',
    description: 'Planlos im Weltraum? Gemeinsam werden wir schlauer!',
    base: '/',
    head: [
        ['link', { rel: 'shortcut icon', href: '/favicon.ico', type: 'image/x-icon', title: 'Favicon' }],
        ['meta', { name: 'theme-color', content: '#337ab7' }],
    ],
    themeConfig: {
        activeHeaderLinks: false,
        enableDarkMode: false,
        nav: [
            {text: 'Portal', link: 'https://www.gigrawars.de'},
            {text: 'Hilfe', link: '/help/intro'},
            {text: 'Erweitert', link: '/more/newbie'},
            {text: 'Rechtliches', link: '/legal/rules'},
        ],
        sidebar: {
            '/help/': [
                {
                    title: 'Einführung',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/intro',
                    ]
                },
                {
                    title: 'Tutorials',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/first-steps',
                        '/help/accounte-save',
                    ]
                },
                {
                    title: 'Planet',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/planet',
                        '/help/building',
                        '/help/research',
                        '/help/units',
                        '/help/resource',
                    ]
                },
                {
                    title: 'Universum',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/fleet',
                        '/help/targetlist',
                        '/help/simulator',
                        '/help/galaxy',
                    ]
                },
                {
                    title: 'Community',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/messages',
                        '/help/alliance',
                        '/help/highscore'
                    ]
                },
                {
                    title: 'Account',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/help/statistics',
                        '/help/settings',
                    ]
                }
            ],
            '/more/': [
                {
                    title: 'Spielmechaniken',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/more/newbie',
                        '/more/battlesystem',
                        '/more/faq',
                    ]
                },
                {
                    title: 'Events',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/more/events',
                        '/more/war',
                        '/more/bounty',
                    ]
                },
                {
                    title: 'Extern',   // required
                    collapsable: false, // optional, defaults to true
                    sidebarDepth: 0,    // optional, defaults to 1
                    children: [
                        '/more/discord',
                        '/more/support',
                        '/more/kb-tool',
                        '/more/assistance',
                        '/more/external-sites',
                    ]
                },
            ],
            '/legal/': [
                '/legal/rules',
                '/legal/rules_old',
                '/legal/agb',
                '/legal/privacy',
                '/legal/imprint',
            ]
        }
    },
    plugins: {
        'sitemap': {
            hostname: 'https://info.gigrawars.de',
            exclude: ['/404.html']
        },
    }
}
