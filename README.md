# GigraWars Info

GigraWars Info contains help and other useful information about the game GigraWars.

## Install

If you use nvm, you can select the appropriate node version with `nvm install`.
This Project used currently node v16.14.2 (npm v8.5.0).

Run `npm install` to install the dependencies so that the documentation can be built.

### Develop

```
npm run docs:dev
```

### Build

```
npm run docs:build
```
